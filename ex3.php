<?php
function tradeElements($string){
    $element = $string[0];
    $string[0] = $string[-1];
    $string[-1] = $element;

    return $string;
    //echo $string ."<br>"
}

$string1 = "ab";
$string2 = "azerty";
$string3 = "s";

print_r($string1);
echo "<br>";
print_r($string2);
echo "<br>";
print_r($string3);
echo "<br>";

echo "<br>";

echo tradeElements($string1);
echo "<br>";
echo tradeElements($string2);
echo "<br>";
echo tradeElements($string3);
//print_r must be used for arrays but not for strings.
?>