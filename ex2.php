<?php
$factor = 5;

echo "<ul>";
for ($i = 1; $i <= 10; $i++) {
    echo "<li> $factor x $i = " .($factor*$i). "</li>";
}
echo "</ul>";
echo "<br>";
?>

<!DOCTYPE html>

<html lang="fr">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="witdth=device-width, initial-scale=1"/>
        <meta name="description" content="PHP Basics Exercices"/> 

        <title>PHP Basics: Inline PHP code in HTML page</title>
    </head>

    <body>
        <ul>
            <?php
                $factor = 5;
                for ($i = 1; $i <= 10; $i++) {
                    echo "<li> $factor x $i = " .($factor*$i). "</li>";
                }
            ?>
        </ul>
    </body>
</html>