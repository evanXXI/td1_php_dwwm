<?php
function isPalindrome($string) {
    $reverseString = strrev($string);
    
    if ($string == $reverseString) {
        echo "$string est bien un palindrome. <br>";
    }else {
        echo "$string n'est pas un palindrome. Son inverse est $reverseString <br>";
    }
}

$string1 = "bob";
$string2 = "radar";
$string3 = "chaussette";
$string4 = "coloc";
$string4 = "kayak";

isPalindrome($string1);
isPalindrome($string2);
isPalindrome($string3);
?>