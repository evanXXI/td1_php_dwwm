<?php
$apple = 3;
$banana = 12;
/*
var_dump($apple);
var_dump($banana);

    On peut vérifier le type de nos variables avec des var_dump de test.
/*

/*
echo "J'ai " .$apple. " pommes et " .$banana. " bananes, j'ai donc " .$apple+$banana. " fruits.";

    ATTENTION ! 
    Si l'on fait ça, le programme effectue d'abord l'opération avant de la concaténer avec le reste de la phrase. Ça crée un conflit de types, donc la page PHP va afficher une erreur. La bonne syntaxe est celle qui suit : 
*/

echo "J'ai $apple pommes et $banana bananes, j'ai donc " .($apple+$banana). " fruits.";

echo "<br>";

echo 'J\'ai ' .$apple. ' pommes et ' .$banana. ' bananes, j\'ai donc ' .($apple + $banana). ' fruits.';