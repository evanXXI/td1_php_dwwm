<?php
for ($i = 0; $i < 5; $i++) {
    //echo "i=$i ";
    for ($j = 5; $i+$j >= 5; $j--){
        //echo "j=$j ";
        echo "* ";
    }
    echo "<br>";
}

// Ici, on incrémenté la valeur de $i et décrémente celle de $j.
// On peut le faire d'une façon plus intuitive avec une condition d'arrêt de $j<=$i et une incrémentation de $j.
?>