<?php
//Ceci est un commentaire sur une ligne
/*
    Ceci est un commenentaire
    Sur plusieurs lignes
*/

$tab = ["Hello", "World"];

var_dump($tab);
echo "<br>";

print_r($tab);
echo "<br>";

print_r($tab[0]);

/*  Ici on affiche un tableau de caractères. 
    La fonction var_dump affichant énormément d'infos, on utilisera la fonction print_r pour un rendu plus lisible.
*/

$toto = 1;
echo "<br>";
var_dump($toto);

echo "<br>";

for ($i = 0; $i < 10; $i++) {
    //echo $i . " ";
    echo $i . "<br>";
    // Concaténation de la valeur de la variable i + un saut de ligne en HTML.
}

foreach ($tab as $val) {
    echo $val . "<br>";
}

$tab2 = ["first" => "val1", "second" => "val2"];

foreach ($tab as $val) {
    echo $val . "<br>";
}

print_r($_GET);
//$_GET récupère les paramètres stockés dans l'URL grâce aux opérateurs ? et &

//print_r($_ENV);
//print_r($_SERVER);
//print_r($_SESSION);
//print_r($_COOKIE);
?>